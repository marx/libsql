package net.sf.jparts.libsql.query.parser;

public class LongConverter extends AbstractIntegerConverter<Long> {

    public LongConverter() {
    }

    public LongConverter(boolean safe, Long errorValue, Long nullValue, int radix) {
        super(safe, errorValue, nullValue, radix);
    }

    @Override
    protected Long tryCreate(String value, int radix) throws NumberFormatException {
        return Long.valueOf(value, radix);
    }
}
