package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObjectTypeHandler implements TypeHandler<Object> {

    @Override
    public Object getResult(ResultSet rs, String column) throws SQLException {
        return rs.getObject(column);
    }
}
