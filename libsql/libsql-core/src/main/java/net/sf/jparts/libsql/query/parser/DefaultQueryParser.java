package net.sf.jparts.libsql.query.parser;

import java.util.*;

import net.sf.jparts.libsql.query.SelectQuery;
import net.sf.jparts.libsql.query.client.*;
import net.sf.jparts.libsql.query.constraints.Constraint;
import net.sf.jparts.libsql.query.constraints.Constraints;

public class DefaultQueryParser implements QueryParser {

    protected Map<String, ParameterInfo> params = new HashMap<>();

    protected Set<String> requiredParams = new HashSet<>();

    protected Map<String, PropertyInfo> properties = new HashMap<>();

    public void registerParameter(String name, String newName) {
        registerParameter(name, new ParameterInfo(name, newName, null, false));
    }

    public void registerParameter(String name, int position) {
        registerParameter(name, new ParameterInfo(name, position, null, false));
    }

    public void registerParameter(String name, String newName, ValueConverter<?> converter) {
        registerParameter(name, new ParameterInfo(name, newName, converter, false));
    }

    public void registerParameter(String name, int position, ValueConverter<?> converter) {
        registerParameter(name, new ParameterInfo(name, position, converter, false));
    }

    public void registerParameter(String name, String newName, boolean required) {
        registerParameter(name, new ParameterInfo(name, newName, null, required));
    }

    public void registerParameter(String name, int position, boolean required) {
        registerParameter(name, new ParameterInfo(name, position, null, required));
    }

    public void registerParameter(String name, String newName, ValueConverter<?> converter, boolean required) {
        registerParameter(name, new ParameterInfo(name, newName, converter, required));
    }

    public void registerParameter(String name, int position, ValueConverter<?> converter, boolean required) {
        registerParameter(name, new ParameterInfo(name, position, converter, required));
    }

    public void registerParameter(String name, ValueConverter<?> converter) {
        registerParameter(name, new ParameterInfo(name, null, converter, false));
    }

    public void registerParameter(String name, boolean required) {
        registerParameter(name, new ParameterInfo(name, null, null, required));
    }

    public void registerParameter(String name, ValueConverter<?> converter, boolean required) {
        registerParameter(name, new ParameterInfo(name, null, converter, required));
    }

    protected void registerParameter(String name, ParameterInfo info) {
        params.put(name, info);
        if (info.required) {
            requiredParams.add(name);
        }
        info.check();
    }

    public void registerProperty(String property, ValueConverter<?> converter) {
        registerProperty(property, new PropertyInfo(property, converter));
    }

    public void registerProperty(String property, String newName, ValueConverter<?> converter) {
        registerProperty(property, new PropertyInfo(property, newName, converter));
    }

    protected void registerProperty(String property, PropertyInfo info) {
        properties.put(property, info);
    }

    /**
     * Configures given {@link SelectQuery} from external <code>config</code>.
     * Paging parameters will be replaced, parameters and constraints
     * will be added.
     *
     * @param config external query configuration, not <code>null</code>.
     * @param query initialized query instance, not <code>null</code>.
     * @param <E> type of objects, selected by <code>query</code>.
     *
     * @return <code>query</code> parameter.
     *
     * @throws IllegalArgumentException if any parameter is <code>null</code>.
     */
    @Override
    public <E> SelectQuery<E> parse(QueryConfig config, SelectQuery<E> query) {
        if (config == null) {
            throw new IllegalArgumentException("config must not be null");
        }
        if (query == null) {
            throw new IllegalArgumentException("query must not be null");
        }
        parsePaging(config, query);
        parseParameters(config, query);
        parseConstraints(config, query);
        return query;
    }

    protected <E> void parsePaging(QueryConfig config, SelectQuery<E> query) {
        query.setFirstResult(config.getFirstResult());
        query.setMaxResults(config.getMaxResults());
    }

    protected <E> void parseParameters(QueryConfig config, SelectQuery<E> query) {
        Set<String> processed = new HashSet<String>();

        for (Parameter p : config.getParameters()) {
            if (params.containsKey(p.getName())) {
                ParameterInfo i = params.get(p.getName());
                Object value = (i.converter != null) ? i.converter.convert(p.getValue()) : p.getValue();
                if (i.position > -1) {
                    query.setParameter(i.position, value);
                } else if (i.newName != null) {
                    query.setParameter(i.newName.trim(), value);
                } else {
                    query.setParameter(p.getName(), value);
                }
            } else {
                query.setParameter(p.getName(), p.getValue());
            }
            processed.add(p.getName());
        }

        Set<String> notProcessed = new HashSet<>();
        notProcessed.addAll(requiredParams);
        notProcessed.removeAll(processed);
        if (notProcessed.size() > 0) {
            throw new IllegalArgumentException("Required parameters are not " +
                    "specified in QueryConfig: " + Arrays.toString(notProcessed.toArray()));
        }
    }

    protected <E> void parseConstraints(QueryConfig config, SelectQuery<E> query) {
        List<Constraint> cs = new ArrayList<>(query.getConstraints());
        cs.addAll(createConstraints(config.getConstraints()));
        query.where(cs);
    }

    protected List<Constraint> createConstraints(List<net.sf.jparts.libsql.query.client.Constraint> cs) {
        List<Constraint> result = new ArrayList<>();
        for (net.sf.jparts.libsql.query.client.Constraint c : cs) {
            Constraint rc = createConstraint(c);
            if (rc != null) {
                result.add(rc);
            }
        }
        return result;
    }

    protected Constraint createConstraint(net.sf.jparts.libsql.query.client.Constraint c) {
        if (c == null) {
            return null;
        }
        if (c instanceof AndConstraint) {
            return Constraints.and(createConstraints(((AndConstraint) c).getConstraints()));
        } else if (c instanceof OrConstraint) {
            return Constraints.or(createConstraints(((OrConstraint) c).getConstraints()));
        } else if (c instanceof ConjunctionConstraint) {
            return Constraints.conjunction(createConstraints(((ConjunctionConstraint) c).getConstraints()));
        } else if (c instanceof DisjunctionConstraint) {
            return Constraints.disjunction(createConstraints(((DisjunctionConstraint) c).getConstraints()));
        } else if (c instanceof NotConstraint) {
            Constraint rc = createConstraint(((NotConstraint) c).getConstraint());
            return (rc != null) ? Constraints.not(rc) : null;
        } else if (c instanceof NullConstraint) {
            return Constraints.isNull(transformProperty(((NullConstraint) c).getProperty()));
        } else if (c instanceof NotNullConstraint) {
            return Constraints.isNotNull(transformProperty(((NotNullConstraint) c).getProperty()));
        } else if (c instanceof SimpleConstraint) {
            SimpleConstraint sc = (SimpleConstraint) c;
            String tp = transformProperty(sc.getProperty());
            Object value = transformValue(sc.getProperty(), sc.getValue());
            switch (sc.getOp()) {
                case EQ: {
                    return Constraints.eq(tp, value);
                }
                case GE: {
                    return Constraints.ge(tp, value);
                }
                case GT: {
                    return Constraints.gt(tp, value);
                }
                case LE: {
                    return Constraints.le(tp, value);
                }
                case LIKE: {
                    return Constraints.like(tp, value);
                }
                case LT: {
                    return Constraints.lt(tp, value);
                }
                case NE: {
                    return Constraints.ne(tp, value);
                }
                default: {
                    throw new IllegalArgumentException(
                            "unsupported operator in SimpleConstraint: " + sc.toString());
                }
            }
        } else if (c instanceof ContainsConstraint) {
            ContainsConstraint cc = (ContainsConstraint) c;
            return Constraints.contains(transformProperty(cc.getProperty()),
                    transformValue(cc.getProperty(), cc.getValue()));
        } else if (c instanceof EqualsIgnoreCaseConstraint) {
            EqualsIgnoreCaseConstraint eic = (EqualsIgnoreCaseConstraint) c;
            return Constraints.equalsIgnoreCase(transformProperty(eic.getProperty()),
                    transformValue(eic.getProperty(), eic.getValue()));
        } else if (c instanceof InIgnoreCaseConstraint) {
            InIgnoreCaseConstraint iuc = (InIgnoreCaseConstraint) c;
            return Constraints.inIgnoreCase(transformProperty(iuc.getProperty()),
                    transformValues(iuc.getProperty(), iuc.getValues()));
        } else if (c instanceof InConstraint) {
            InConstraint ic = (InConstraint) c;
            return Constraints.in(transformProperty(ic.getProperty()),
                    transformValues(ic.getProperty(), ic.getValues()));
        } else if (c instanceof BetweenConstraint) {
            BetweenConstraint b = (BetweenConstraint) c;
            return Constraints.between(
                    transformProperty(b.getProperty()),
                    transformValue(b.getProperty(), b.getLow()),
                    transformValue(b.getProperty(), b.getHigh()));
        } else if (c instanceof StartsWithCaseSensitiveConstraint) {
            StartsWithCaseSensitiveConstraint scs = (StartsWithCaseSensitiveConstraint) c;
            return Constraints.startsWithCs(transformProperty(scs.getProperty()),
                    transformValue(scs.getProperty(), scs.getValue()));
        } else if (c instanceof StartsWithConstraint) {
            StartsWithConstraint s = (StartsWithConstraint) c;
            return Constraints.startsWith(
                    transformProperty(s.getProperty()),
                    transformValue(s.getProperty(), s.getValue()));
        } else if (c instanceof EndsWithCaseSensitiveConstraint) {
            EndsWithCaseSensitiveConstraint ecs = (EndsWithCaseSensitiveConstraint) c;
            return Constraints.endsWithCs(
                    transformProperty(ecs.getProperty()),
                    transformValue(ecs.getProperty(), ecs.getValue()));
        } else if (c instanceof EndsWithConstraint) {
            EndsWithConstraint e = (EndsWithConstraint) c;
            return Constraints.endsWith(
                    transformProperty(e.getProperty()),
                    transformValue(e.getProperty(), e.getValue()));
        }
        throw new IllegalStateException("unsupported constraint type: " + c.getClass().getName());
    }

    protected String transformProperty(String property) {
        PropertyInfo i = properties.get(property);
        if (i != null) {
            return (i.newName == null || i.newName.isEmpty()) ? property : i.newName;
        }
        return property;
    }

    protected List<Object> transformValues(String property, List<String> values) {
        List<Object> result = new ArrayList<>();
        for (String v : values) {
            result.add(transformValue(property, v));
        }
        return result;
    }

    protected Object transformValue(String property, String value) {
        PropertyInfo i = properties.get(property);
        if (i != null) {
            return (i.converter != null) ? i.converter.convert(value) : value;
        }
        return value;
    }

    public class ParameterInfo {

        public String name;

        public String newName;

        public int position = -1;

        public ValueConverter<?> converter;

        public boolean required = false;

        public ParameterInfo() {
        }

        public ParameterInfo(String name, String newName, ValueConverter<?> converter, boolean required) {
            this.name = name;
            this.newName = newName;
            this.converter = converter;
            this.required = required;
        }

        public ParameterInfo(String name, int position, ValueConverter<?> converter, boolean required) {
            this.name = name;
            this.position = position;
            this.converter = converter;
            this.required = required;
        }

        public void check() {
            if (name == null || name.isEmpty()) {
                throw new IllegalStateException("name must not be empty or null");
            }
            if ((newName != null && newName.length() > 0) && position > -1) {
                throw new IllegalStateException("parameter \"" + name + "\" can't have both \"newName\" and \"position\".");
            }
        }
    }

    public class PropertyInfo {

        public String name;

        public String newName;

        public ValueConverter<?> converter;

        public PropertyInfo() {
        }

        public PropertyInfo(String name, ValueConverter<?> converter) {
            this.name = name;
            this.converter = converter;
        }

        public PropertyInfo(String name, String newName, ValueConverter<?> converter) {
            this.name = name;
            this.newName = newName;
            this.converter = converter;
        }
    }
}
