package net.sf.jparts.libsql.query.constraints;

public class PropertyConstraint implements Constraint {

    private final String propertyName;

    private final String otherPropertyName;

    private final String operator;

    protected PropertyConstraint(String propertyName, String operator, String otherPropertyName) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        if (operator == null || operator.trim().isEmpty()) {
            throw new IllegalArgumentException("operator must not be empty string or null");
        }
        if (otherPropertyName == null || otherPropertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("otherPropertyName must not be empty string or null");
        }
        this.propertyName = propertyName.trim();
        this.operator = operator.trim();
        this.otherPropertyName = otherPropertyName.trim();
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getOtherPropertyName() {
        return otherPropertyName;
    }

    public String getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        return propertyName + " " + operator + " " + otherPropertyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PropertyConstraint that = (PropertyConstraint) o;

        if (operator != null ? !operator.equals(that.operator) : that.operator != null) return false;
        if (otherPropertyName != null ? !otherPropertyName.equals(that.otherPropertyName) : that.otherPropertyName != null)
            return false;
        //noinspection RedundantIfStatement
        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (otherPropertyName != null ? otherPropertyName.hashCode() : 0);
        result = 31 * result + (operator != null ? operator.hashCode() : 0);
        return result;
    }
}
