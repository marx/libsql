package net.sf.jparts.libsql.query.constraints;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractJoinConstraint implements Constraint {

    protected final List<Constraint> constraints = new ArrayList<>();

    protected String joinOperator = ", ";

    protected boolean appendSpaces = false;

    protected boolean group = false;

    protected AbstractJoinConstraint setConstraints(Constraint... constraints) {
        this.constraints.clear();
        if (constraints != null) {
            for (Constraint c : constraints) {
                if (c != null) {
                    this.constraints.add(c);
                }
            }
        }
        return this;
    }

    public List<Constraint> getConstraints() {
        return new ArrayList<>(constraints);
    }

    public String getJoinOperator() {
        return joinOperator;
    }

    public boolean isAppendSpaces() {
        return appendSpaces;
    }

    public boolean isGroup() {
        return group;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (constraints.size() > 0) {
            if (group) {
                sb.append("(");
            }
            for (int i = 0; i < constraints.size(); i++) {
                if (i > 0) {
                    sb.append((appendSpaces) ? " " + joinOperator + " " : joinOperator);
                }
                sb.append(constraints.get(i));
            }
            if (group) {
                sb.append(")");
            }
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractJoinConstraint that = (AbstractJoinConstraint) o;

        if (appendSpaces != that.appendSpaces) return false;
        if (group != that.group) return false;
        if (constraints != null ? !constraints.equals(that.constraints) : that.constraints != null) return false;
        //noinspection RedundantIfStatement
        if (joinOperator != null ? !joinOperator.equals(that.joinOperator) : that.joinOperator != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = constraints != null ? constraints.hashCode() : 0;
        result = 31 * result + (joinOperator != null ? joinOperator.hashCode() : 0);
        result = 31 * result + (appendSpaces ? 1 : 0);
        result = 31 * result + (group ? 1 : 0);
        return result;
    }
}
