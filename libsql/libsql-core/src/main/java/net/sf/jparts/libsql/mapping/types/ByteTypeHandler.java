package net.sf.jparts.libsql.mapping.types;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ByteTypeHandler implements TypeHandler<Byte> {

    @Override
    public Byte getResult(ResultSet rs, String column) throws SQLException {
        byte b = rs.getByte(column);
        return (rs.wasNull()) ? null : b;
    }
}
