package net.sf.jparts.libsql.mapping.builder;

import net.sf.jparts.libsql.mapping.ResultMapping;

public class ClassMappingBuilder {

    public ResultMapping build(String id, Class<?> clazz) {
        return new ResultMapping.Builder(id)
                .setType(clazz)
                .build();
    }
}
