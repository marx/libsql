package net.sf.jparts.libsql.loader;

import net.sf.jparts.libsql.common.Configuration;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default {@link QueryLoader} implementation; loads query string from file available in classpath.
 * Support query string caching.
 * <p>
 * Example of query name processing:
 * <br>if query name is <code>findAllUsers</code> then loader try
 * to read contents of file named <code>META-INF/libsql/queries/findAllUsers.sql</code>.
 */
public class DefaultQueryLoader implements QueryLoader {

    /**
     * Logger.
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Defines cache state configuration property.
     */
    public static final String CACHE_ENABLED_KEY = "cache";

    /**
     * Defines initial capacity configuration property for cache table.
     */
    public static final String CACHE_INITIAL_CAPACITY_KEY = "cache.initialCapacity";

    /**
     * Defines load factor configuration property for cache table;
     */
    public static final String CACHE_LOAD_FACTOR_KEY = "cache.loadFactor";

    /**
     * Defines cache concurrency level for cache table.
     */
    public static final String CACHE_CONCURRENCY_LEVEL_KEY = "cache.concurrencyLevel";

    /**
     * The default initial capacity for cache table,
     * used when not otherwise specified in a configuration.
     */
    public static final int DEFAULT_INITIAL_CAPACITY = 60;

    /**
     * The default load factor for cache table, used when not
     * otherwise specified in a configuration.
     */
    public static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The default concurrency level for cache table, used when not
     * otherwise specified in a configuration.
     */
    public static final int DEFAULT_CONCURRENCY_LEVEL = 16;

    /**
     * Cache state, cache enabled if it is <code>true</code>.
     */
    protected boolean cacheEnabled;

    /**
     * Initial capacity of cache table is read from configuration.
     * Changes of this field does not reflect cache table if it is already created.
     */
    protected int initialCapacity;

    /**
     * Load factor of cache table is read from configuration.
     * Changes of this field does not reflect cache table if it is already created.
     */
    protected float loadFactor;

    /**
     * Concurrency level of cache table is read from configuration.
     * Changes of this field does not reflect cache table if it is already created.
     */
    protected int concurrencyLevel;

    /**
     * Cache for query strings. Key - query name, value - query string.
     */
    protected Map<String, String> queryCache;

    /**
     * Encoding for query files.
     */
    protected String fileEncoding;

    /**
     * Default constructor. Execute {@link #initCacheConfiguration(Properties)}.
     */
    public DefaultQueryLoader() {
        Properties p = Configuration.get(this.getClass());
        initEncoding(p);
        initCacheConfiguration(p);
    }

    protected void initEncoding(Properties p) {
        fileEncoding = Configuration.getPropertyValue(p, "encoding", "UTF-8");
    }

    /**
     * Read configuration from given <code>properties</code>.
     *
     * @param properties an instance of {@link Properties}, it can be <code>null</code>.
     */
    protected void initCacheConfiguration(final Properties properties) {
        cacheEnabled = Configuration.getBooleanPropertyValue(properties, CACHE_ENABLED_KEY, true);

        final String sInitialCapacity = Configuration.getPropertyValue(properties, CACHE_INITIAL_CAPACITY_KEY, "");
        final String sLoadFactor = Configuration.getPropertyValue(properties, CACHE_LOAD_FACTOR_KEY, "");
        final String sConcurrencyLevel = Configuration.getPropertyValue(properties, CACHE_CONCURRENCY_LEVEL_KEY, "");

        try {
            initialCapacity = Integer.parseInt(sInitialCapacity);
        } catch (NumberFormatException ex) {
            initialCapacity = DEFAULT_INITIAL_CAPACITY;
        }

        try {
            loadFactor = Float.parseFloat(sLoadFactor);
        } catch (NumberFormatException ex) {
            loadFactor = DEFAULT_LOAD_FACTOR;
        }

        try {
            concurrencyLevel = Integer.parseInt(sConcurrencyLevel);
        } catch (NumberFormatException ex) {
            concurrencyLevel = DEFAULT_CONCURRENCY_LEVEL;
        }

        if (cacheEnabled) {
            queryCache = new ConcurrentHashMap<>(initialCapacity, loadFactor, concurrencyLevel);
        }

        // log cache configuration
        logger.info("\nlog cache configuration:\n\tinitialCapacity={}" +
                "\n\tconcurrencyLevel={}\n\tloadFactor={}\n\tcacheEnabled={}",
                initialCapacity, concurrencyLevel, loadFactor, cacheEnabled);
    }

    /**
     * Returns <code>true</code> if cache of query strings is enabled.
     *
     * @return <core>true</code> if cache is enabled, otherwise <code>false</code>.
     */
    public boolean isCacheEnabled() {
        return cacheEnabled;
    }

    /**
     * Enables or disables caching of query strings at runtime.
     * Cache table will be initialized if it was not created yet and cache state is set to enabled.
     *
     * @param cacheEnabled pass <code>true</code> to enable cache, <code>false</code> to disable.
     */
    public synchronized void setCacheEnabled(boolean cacheEnabled) {
        if (cacheEnabled && queryCache == null) {
            queryCache = new ConcurrentHashMap<>(initialCapacity, loadFactor, concurrencyLevel);
        }

        this.cacheEnabled = cacheEnabled;
    }

    /**
     * Clears query string cache. Do nothing if cache is disabled.
     */
    public synchronized void clearCache() {
        if (queryCache != null) {
            queryCache.clear();
        }
    }

    /**
     * Remove query string from cache. Do nothing if cache is disabled.
     *
     * @param queryName name of query.
     * @throws IllegalArgumentException if <code>queryName</code> is empty or <code>null</code>.
     */
    public void removeFromCache(String queryName) {
        if (queryName == null || queryName.isEmpty()) {
            throw new IllegalArgumentException("queryName must not be empty or null");
        }
        if (queryCache != null) {
            queryCache.remove(queryName);
        }
    }

    /**
     * Loads given list of queries to cache. Does not throw {@link QueryLoadException} but returns
     * exception instances in result map. It may be useful if your application have customized initialization
     * service.
     *
     * @param queries collection of query names to load and cache.
     * @return map of query names and exceptions, occurred at load time. Empty if all queries
     *         have been loaded successfully. Not <code>null</code>.
     * @throws IllegalArgumentException if <code>queries</code> is <code>null</code>.
     * @throws IllegalStateException if cache is disabled.
     */
    public Map<String, QueryLoadException> preloadQueries(Collection<String> queries) {
        if (queries == null) {
            throw new IllegalArgumentException("collection of queries must not be null");
        }
        if (!cacheEnabled) {
            throw new IllegalStateException("cache is disabled");
        }
        Map<String, QueryLoadException> failed = new HashMap<>();
        for (String queryName : queries) {
            if (queryName == null || queryName.isEmpty()) {
                continue;
            }
            try {
                String q = load0(queryName);
                queryCache.put(queryName, q);
            } catch (QueryLoadException ex) {
                // noinspection ThrowableResultOfMethodCallIgnored
                failed.put(queryName, ex);
            }
        }
        return failed;
    }

    /**
     * Loads query from classpath. Does not check argument value, it is must already done in {@link #load(String)}.
     *
     * @param qName query name.
     * @return query string.
     * @throws QueryLoadException if runtime error occurred.
     */
    protected String load0(String qName) {
        // filename for query
        final String qf = "META-INF/libsql/queries/" + qName + ".sql";
        // failure message
        final String failMsg = "Unable to load query from \"" + qf + "\"";

        // open stream
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        InputStream in = cl.getResourceAsStream(qf);

        if (in == null) {
            logger.error("Unable to read query from {} - InputStream is null", qf);
            throw new QueryLoadException(failMsg + " - InputStream is null");
        }

        // read file contents
        Reader reader = null;
        StringWriter writer = new StringWriter();
        try {
            reader = new InputStreamReader(in, fileEncoding);
            char[] buffer = new char[5120];

            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            writer.flush();
        } catch (IOException ex) {
            logger.error("Unable to read query from {}", qf, ex);
            throw new QueryLoadException(failMsg, ex);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
                logger.error("Unable to close input stream reader", ex);
            }
        }

        String q = writer.toString();
        q = q.trim();

        // empty query is useless
        if (q.length() < 1) {
            throw new QueryLoadException("Query in \"" + qf + "\" is empty");
        }

        return q;
    }

    public String load(String queryName) {
        if (queryName == null || queryName.trim().length() < 1) {
            throw new IllegalArgumentException("queryName must not be empty or null");
        }

        boolean lCacheEnabled = cacheEnabled;
        final String qName = queryName.trim();

        // get query from cache
        if (lCacheEnabled) {
            String q = queryCache.get(qName);
            if (q != null && q.length() > 0) {
                return q;
            }
        }

        String q = load0(qName);

        if (lCacheEnabled) {
            queryCache.put(qName, q);
        }

        return q;
    }
}
