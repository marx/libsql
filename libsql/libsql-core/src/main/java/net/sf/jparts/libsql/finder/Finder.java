package net.sf.jparts.libsql.finder;

import net.sf.jparts.libsql.query.SelectQuery;

public interface Finder {

    /**
     * Finder must not calcute total count of rows in result if hint is present in query.
     */
    public static final String NO_TOTAL_LENGTH_HINT = "noTotalLength";

    public <E> FinderResult<E> find(final SelectQuery<E> query);
}
