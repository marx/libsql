package net.sf.jparts.libsql.query.constraints;

public class BetweenConstraint implements OnePropertyConstraint {

    private final String propertyName;

    private final Object low;

    private final Object high;

    protected BetweenConstraint(String propertyName, Object low, Object high) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("propertyName must not be empty string or null");
        }
        if (low == null) {
            throw new IllegalArgumentException("low value must not be null");
        }
        if (high == null) {
            throw new IllegalArgumentException("high value must not be null");
        }

        this.propertyName = propertyName.trim();
        this.low = low;
        this.high = high;
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    public Object getLow() {
        return low;
    }

    public Object getHigh() {
        return high;
    }

    @Override
    public String toString() {
        return propertyName + " between " + low + " and " + high;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BetweenConstraint that = (BetweenConstraint) o;

        if (high != null ? !high.equals(that.high) : that.high != null) return false;
        if (low != null ? !low.equals(that.low) : that.low != null) return false;
        //noinspection RedundantIfStatement
        if (propertyName != null ? !propertyName.equals(that.propertyName) : that.propertyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = propertyName != null ? propertyName.hashCode() : 0;
        result = 31 * result + (low != null ? low.hashCode() : 0);
        result = 31 * result + (high != null ? high.hashCode() : 0);
        return result;
    }
}
