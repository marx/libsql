package net.sf.jparts.libsql.mapping.builder.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscriminatorCase")
public class CaseTag {

    @XmlAttribute(name = "value", required = true)
    public String value;

    @XmlAttribute(name = "result", required = true)
    public String result;

}
