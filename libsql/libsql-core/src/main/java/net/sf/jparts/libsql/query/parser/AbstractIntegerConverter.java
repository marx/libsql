package net.sf.jparts.libsql.query.parser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract public class AbstractIntegerConverter<T> implements ValueConverter<T> {

    public static final int DEFAULT_RADIX = 10;

    private Logger logger = LoggerFactory.getLogger(ValueConverter.LOGGER_NAME);

    private final T errorValue;

    private final T nullValue;

    private final boolean safe;

    private final int radix;

    public AbstractIntegerConverter() {
        this(true, null, null, DEFAULT_RADIX);
    }

    public AbstractIntegerConverter(boolean safe, T errorValue, T nullValue, int radix) {
        this.safe = safe;
        this.errorValue = errorValue;
        this.nullValue = nullValue;
        this.radix = radix;
    }

    @Override
    public T convert(String value) {
        if (value == null || value.isEmpty()) return nullValue;

        try {
            return tryCreate(value, radix);
        } catch (NumberFormatException ex) {
            if (safe) {
                logger.debug("Can't convert, value = {}", value, ex);
                return errorValue;
            }
            throw new ValueFormatException("Can't convert, value = " + value, ex);
        }
    }

    abstract protected T tryCreate(String value, int radix) throws NumberFormatException;
}
