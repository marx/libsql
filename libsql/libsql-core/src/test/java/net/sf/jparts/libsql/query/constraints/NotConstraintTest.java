package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link NotConstraint}.
 */
public class NotConstraintTest {

    private static NotConstraint nc;

    @BeforeClass
    public static void setUpBeforeClass() {
        nc = new NotConstraint(new TxtConstraint("txt"));
    }

    @AfterClass
    public static void tearDownAfterClass() {
        nc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new NotConstraint(null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }

    @Test
    public void testGetConstraint() {
        Constraint c = nc.getConstraint();
        assertNotNull(nc);
        assertTrue(c instanceof TxtConstraint);
        assertEquals("txt", c.toString());
    }

    @Test
    public void testToString() {
        assertEquals("not txt", nc.toString());
    }
}
