package net.sf.jparts.libsql.query;

import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class QueryParameterImplTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testParams_WithSameName() {
        SelectQuery<?> sq = new SelectQueryImpl<>();
        sq.setParameter("name", "v1");
        sq.setParameter("name", "v2");

        Set<QueryParameter<?>> params = sq.getParameters();
        assertEquals(1, params.size());
        QueryParameter<?> p1 = params.iterator().next();
        assertNotNull(p1);
        assertEquals("name", p1.getName());
        assertEquals("v2", sq.getParameterValue(p1));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testParams_WithSamePosition() {
        final Integer pos = 22;
        SelectQuery<?> sq = new SelectQueryImpl<>();
        sq.setParameter(pos, "v1");
        sq.setParameter(pos, "v2");

        Set<QueryParameter<?>> params = sq.getParameters();
        assertEquals(1, params.size());
        QueryParameter<?> p1 = params.iterator().next();
        assertNotNull(p1);
        assertEquals(pos, p1.getPosition());
        assertEquals("v2", sq.getParameterValue(p1));
    }

}
