package net.sf.jparts.libsql.mapping.types;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrimitivesTest {

    @Test
    public void testGetDefaultValue() {
        assertEquals(false, Primitives.getDefaultValue(boolean.class));
        assertEquals('\0', Primitives.getDefaultValue(char.class));
        assertEquals((byte) 0, Primitives.getDefaultValue(byte.class));
        assertEquals((short) 0, Primitives.getDefaultValue(short.class));
        assertEquals(0, Primitives.getDefaultValue(int.class));
        assertEquals(0L, Primitives.getDefaultValue(long.class));
        assertEquals(0.0f, Primitives.getDefaultValue(float.class));
        assertEquals(0.0d, Primitives.getDefaultValue(double.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDefaultValue_UnsupportedClass() {
        Primitives.getDefaultValue(String.class);
    }

    @Test
    public void testGetOrDefault() {
        assertEquals(false, Primitives.getOrDefault(boolean.class, null));
        assertEquals('\0', Primitives.getOrDefault(char.class, null));
        assertEquals((byte) 0, Primitives.getOrDefault(byte.class, null));
        assertEquals((short) 0, Primitives.getOrDefault(short.class, null));
        assertEquals(0, Primitives.getOrDefault(int.class, null));
        assertEquals(0L, Primitives.getOrDefault(long.class, null));
        assertEquals(0.0f, Primitives.getOrDefault(float.class, null));
        assertEquals(0.0d, Primitives.getOrDefault(double.class, null));
    }
}
