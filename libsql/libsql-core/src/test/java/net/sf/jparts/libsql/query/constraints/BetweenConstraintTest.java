package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link BetweenConstraint}.
 */
public class BetweenConstraintTest {

    private static BetweenConstraint bc;

    @BeforeClass
    public static void setUpBeforeClass() {
        bc = new BetweenConstraint("p", "1", "2");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        bc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new BetweenConstraint(null, "1", "2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new BetweenConstraint(" ", "1", "2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new BetweenConstraint("p", null, "2");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new BetweenConstraint("p", "1", null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }

    @Test
    public void testGetPropertyName() {
        assertEquals("p", bc.getPropertyName());
    }

    @Test
    public void testGetLow() {
        assertEquals("1", bc.getLow());
    }

    @Test
    public void testGetHigh() {
        assertEquals("2", bc.getHigh());
    }

    @Test
    public void testToString() {
        final String ok = "p between 1 and 2";
        assertEquals(ok, bc.toString());
    }
}
