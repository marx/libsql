package net.sf.jparts.libsql.query.client;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QueryConfigJsonMarshallTest {

    private ObjectMapper mapper = createMapper();

    @Test
    public void testJsonMarshall() throws Exception{
        final String jsonOk = "{\"constraints\":[{\"isNull\":{\"value\":\"propToTestNull\"}}]," +
                "\"firstResult\":0,\"maxResults\":2147483647}";

        QueryConfig qc = new QueryConfig();
        qc.getConstraints().add(new NullConstraint("propToTestNull"));

        String json = mapper.writeValueAsString(qc);

        assertEquals(jsonOk, json);
    }

    @Test
    public void testJsonUnmarshall() throws Exception {
        String json = "{\"constraints\":[{\"isNull\":{\"value\":\"propToTestNull\"}}]}";

        QueryConfig restored = mapper.readValue(json, QueryConfig.class);

        assertEquals(0, restored.getFirstResult());
        assertEquals(Integer.MAX_VALUE, restored.getMaxResults());
        assertTrue(restored.getOrders().isEmpty());
        assertTrue(restored.getParameters().isEmpty());

        assertEquals(1, restored.getConstraints().size());

        Constraint c = restored.getConstraints().get(0);
        assertTrue(c instanceof NullConstraint);
        assertEquals("propToTestNull", ((NullConstraint) c).getProperty());
    }

    private ObjectMapper createMapper() {
        ObjectMapper m = new ObjectMapper();
        m.registerModule(new JaxbAnnotationModule());
        m.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
        m.disable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS);
        return m;
    }
}
