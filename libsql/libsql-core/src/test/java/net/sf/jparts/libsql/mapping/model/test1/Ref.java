package net.sf.jparts.libsql.mapping.model.test1;

public class Ref {

    private String id;

    private String dn;

    public Ref(String id, String dn) {
        this.id = id;
        this.dn = dn;
    }

    public String getId() {
        return id;
    }

    public String getDn() {
        return dn;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Ref");
        sb.append("{id='").append(id).append('\'');
        sb.append(", dn='").append(dn).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
