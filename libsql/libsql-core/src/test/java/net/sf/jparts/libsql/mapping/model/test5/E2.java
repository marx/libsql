package net.sf.jparts.libsql.mapping.model.test5;

public class E2 {

    private E2N n;

    public E2N getN() {
        return n;
    }

    @Override
    public String toString() {
        return "E2{" +
                "n=" + n +
                '}';
    }
}
