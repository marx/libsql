package net.sf.jparts.libsql.mapping.model.test2;

public class Auto {

    public static final String SUFFIX = "__suffix";

    private String id;

    public Auto(String id) {
        this.id = (id == null) ? SUFFIX : id + SUFFIX;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Auto");
        sb.append("{id='").append(id).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
