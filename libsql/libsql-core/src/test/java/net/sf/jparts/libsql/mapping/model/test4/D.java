package net.sf.jparts.libsql.mapping.model.test4;

public class D {

    private String value;

    public D(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("D");
        sb.append("{value='").append(value).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
