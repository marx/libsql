package net.sf.jparts.libsql.finder;

import java.util.Arrays;

import org.junit.Assert;
import net.sf.jparts.libsql.AbstractMarshallTest;
import org.junit.Test;

public class FinderResultMarshallTest extends AbstractMarshallTest {

    private static final String finderXml =
            "<ns2:finderResult xmlns:ns2=\"http://jparts.sf.net/libsql/finder\">" +
            "<offset>0</offset><totalLength>10</totalLength>" +
            "<dataList>" +
            "<data xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">s1</data>" +
            "<data xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">s2</data>" +
            "<data xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">s3</data>" +
            "</dataList>" +
            "</ns2:finderResult>";
//            "<finderResult>" +
//            "<offset>0</offset>" +
//            "<totalLength>10</totalLength>" +
//            "<data>" +
//            "<data xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">s1</data>" +
//            "<data xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">s2</data>" +
//            "<data xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">s3</data>" +
//            "</data>" +
//            "</finderResult>";

    @Test
    public void testMarshall() {
        FinderResult<String> r = new FinderResult<String>(0, 10, Arrays.asList("s1", "s2", "s3"));
        Assert.assertEquals(finderXml, marshall(r));
    }
}
