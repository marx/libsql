package net.sf.jparts.libsql.query.constraints;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for {@link SimpleConstraint}.
 */
public class SimpleConstraintTest {

    private static SimpleConstraint sc;

    @BeforeClass
    public static void setUpBeforeClass() {
        sc = new SimpleConstraint("p1", "o", "v");
    }

    @AfterClass
    public static void tearDownAfterClass() {
        sc = null;
    }

    @Test
    public void testConstructor() {
        try {
            new SimpleConstraint(null, "o", "v");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SimpleConstraint(" ", "o", "v");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SimpleConstraint("p1", null, "v");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SimpleConstraint("p1", "  ", "v");
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }

        try {
            new SimpleConstraint("p1", "o", null);
            fail("IllegalArgumentException expected");
        } catch (IllegalArgumentException ex) {
            // it's ok
        }
    }

    @Test
    public void testGetPropertyName() {
        assertEquals("p1", sc.getPropertyName());
    }

    @Test
    public void testGetValue() {
        assertEquals("v", sc.getValue());
    }

    @Test
    public void testGetOperator() {
        assertEquals("o", sc.getOperator());
    }

    @Test
    public void testToString() {
        assertEquals("p1 o v", sc.toString());
    }
}
