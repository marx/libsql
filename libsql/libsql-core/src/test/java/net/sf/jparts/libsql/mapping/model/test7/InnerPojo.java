package net.sf.jparts.libsql.mapping.model.test7;

public class InnerPojo {

    private int f1 = -5;
    private int f2 = -6;

    private boolean b = true;
    private byte bt = -12;
    private short s = -22;
    private long l = -1111;
    private float f = -110.1f;
    private double d = -21.2d;

    public boolean isB() {
        return b;
    }

    public void setB(boolean b) {
        this.b = b;
    }

    public byte getBt() {
        return bt;
    }

    public void setBt(byte bt) {
        this.bt = bt;
    }

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public int getF1() {
        return f1;
    }

    public void setF1(int f1) {
        this.f1 = f1;
    }

    public int getF2() {
        return f2;
    }

    public void setF2(int f2) {
        this.f2 = f2;
    }

    public float getF() {
        return f;
    }

    public void setF(float f) {
        this.f = f;
    }

    public long getL() {
        return l;
    }

    public void setL(long l) {
        this.l = l;
    }

    public short getS() {
        return s;
    }

    public void setS(short s) {
        this.s = s;
    }

    @Override
    public String toString() {
        return "InnerPojo{" +
                "b=" + b +
                ", f1=" + f1 +
                ", f2=" + f2 +
                ", bt=" + bt +
                ", s=" + s +
                ", l=" + l +
                ", f=" + f +
                ", d=" + d +
                '}';
    }
}
