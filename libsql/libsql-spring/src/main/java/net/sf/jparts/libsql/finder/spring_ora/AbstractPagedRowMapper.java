package net.sf.jparts.libsql.finder.spring_ora;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.sf.jparts.libsql.finder.FinderResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

/**
 * RowMapper implementation that reads total count from special column with name
 * {@link OracleQueryTransformer#TOTAL_LENGTH_COLUMN}. Should be used for {@link OracleSpringJdbcFinder}.
 */
abstract public class AbstractPagedRowMapper<T> implements RowMapper<T> {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected int totalLength = 0;

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException {
        readTotalLength(rs, rowNum);
        return mapRowInternal(rs, rowNum);
    }

    protected void readTotalLength(ResultSet rs, int rowNum) throws SQLException {
        if (rowNum > 0) {
            return;
        }

        try {
            Object tl = rs.getObject(OracleQueryTransformer.TOTAL_LENGTH_COLUMN);
            if (tl != null && tl instanceof Number) {
                totalLength = ((Number) tl).intValue();
            }
        } catch (SQLException ex) {
            totalLength = FinderResult.NO_TOTAL_LENGTH;
            logger.trace(ex.getMessage(), ex);
        }
    }

    abstract public T mapRowInternal(ResultSet rs, int rowNum) throws SQLException;

    public int getTotalLength() {
        return totalLength;
    }
}
