package net.sf.jparts.libsql.spring.template;

/**
 * Used in {@link AdvancedNamedParameterJdbcTemplate} to modify query.
 *
 * <p>Instances can modify the query in any way.
 *
 * <p>Implementations must be thread-safe.
 */
public interface QueryModifier {

    /**
     * Perform query modification.
     *
     * @param sql query, not {@code null}.
     * @return modified query, not {@code null}.
     *
     * @throws QueryModificationException if modification cannot be done.
     */
    public String apply(String sql) throws QueryModificationException;
}
